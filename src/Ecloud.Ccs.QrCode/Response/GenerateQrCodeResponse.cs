using System;
using Ecloud.Ccs.Core.Response;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Ecloud.Ccs.QrCode.Response
{
    [XmlRoot("generate_qr_code_response")]
    public class GenerateQrCodeResponse : EcResponse
    {
        /// <summary>
        /// base64代码的图片
        /// </summary>
        [XmlElement("base64_image")]
        public string Base64Image { get; set; }



    }
}