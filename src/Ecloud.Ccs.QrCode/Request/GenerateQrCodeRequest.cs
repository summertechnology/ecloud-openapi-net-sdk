using System;
using System.Collections.Generic;
using Ecloud.Ccs.Core.Util;
using Ecloud.Ccs.Core.Response;
using Ecloud.Ccs.Core;
using Ecloud.Ccs.QrCode.Response;

namespace Ecloud.Ccs.QrCode.Request
{
    /// <summary>
    /// 生成二维码
    /// </summary>
    public class GenerateQrCodeRequest : BaseEcRequest<GenerateQrCodeResponse>
    {
        /// <summary>
        /// 二维码内容
        /// </summary>
        public string QrCodeContent { get; set; }

        /// <summary>
        /// 宽度像素
        /// </summary>
        public long? Pixel { get; set; }



        public override string GetApiName()
        {
            return "ecloud.qrcode.generate";
        }

        public override void Validate()
        {
        }

        public override IDictionary<string, string> GetParameters()
        {
            var parameters = new EcDictionary();
            parameters.Add("qr_code_content", QrCodeContent);
            parameters.Add("pixel", Pixel);

            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }

            return parameters;
        }

    }
}