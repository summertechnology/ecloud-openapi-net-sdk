using Ecloud.Ccs.Core.Response;
using System.Xml.Serialization;

namespace Ecloud.Ccs.ShortUrl.Response
{
    [XmlRoot("convert_tiny_url_response")]
    public class ConvertTinyUrlResponse : EcResponse
    {
        /// <summary>
        /// 短网址中的关键字
        /// </summary>
        [XmlElement("tiny_key")]
        public string TinyKey { get; set; }



    }
}