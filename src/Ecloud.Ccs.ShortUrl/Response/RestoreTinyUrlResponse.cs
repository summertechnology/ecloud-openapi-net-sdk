using Ecloud.Ccs.Core.Response;
using System.Xml.Serialization;

namespace Ecloud.Ccs.ShortUrl.Response
{
    [XmlRoot("restore_tiny_url_response")]
    public class RestoreTinyUrlResponse : EcResponse
    {
        [XmlElement("url")]
        public string Url { get; set; }
    }
}