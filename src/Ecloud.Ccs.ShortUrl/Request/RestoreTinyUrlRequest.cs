using Ecloud.Ccs.Core;
using Ecloud.Ccs.ShortUrl.Response;
using System.Collections.Generic;

namespace Ecloud.Ccs.ShortUrl.Request
{
    /// <summary>
    /// 将短网址还原
    /// </summary>
    public class RestoreTinyUrlRequest : BaseEcRequest<RestoreTinyUrlResponse>
    {
        /// <summary>
        /// 原始网址
        /// </summary>
        public string TinyKey { get; set; }



        public override string GetApiName()
        {
            return "ecloud.site.tinyurl.restore";
        }

        public override void Validate()
        {
        }

        public override IDictionary<string, string> GetParameters()
        {
            var parameters = new EcDictionary();
            parameters.Add("tiny_key", TinyKey);

            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }

            return parameters;
        }

    }
}