using Ecloud.Ccs.Core;
using Ecloud.Ccs.ShortUrl.Response;
using System.Collections.Generic;

namespace Ecloud.Ccs.ShortUrl.Request
{
    /// <summary>
    /// 将原始网址转化为短网址
    /// </summary>
    public class ConvertTinyUrlRequest : BaseEcRequest<ConvertTinyUrlResponse>
    {
        /// <summary>
        /// 原始网址
        /// </summary>
        public string Url { get; set; }



        public override string GetApiName()
        {
            return "ecloud.site.tinyurl.convert";
        }

        public override void Validate()
        {
        }

        public override IDictionary<string, string> GetParameters()
        {
            var parameters = new EcDictionary();
            parameters.Add("url", Url);

            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }

            return parameters;
        }

    }
}