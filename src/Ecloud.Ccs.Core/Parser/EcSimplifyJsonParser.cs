﻿using System;
using System.Collections;
using Ecloud.Ccs.Core.Response;
using fastJSON;

namespace Ecloud.Ccs.Core.Parser
{
    public class EcSimplifyJsonParser<T> : EcJsonParser<T> where T : EcResponse
    {
        public override T Parse(string body)
        {
            T rsp = null;

            IDictionary rootJson = JSON.Parse(body) as IDictionary;
            if (rootJson != null)
            {
                IDictionary data = rootJson;
                if (rootJson.Contains(Constants.ERROR_RESPONSE))
                {
                    data = rootJson[Constants.ERROR_RESPONSE] as IDictionary;
                }

                if (data != null)
                {
                    IEcReader reader = new EcSimplifyJsonReader(data);
                    rsp = (T)FromJson(reader, typeof(T));
                }
            }

            if (rsp == null)
            {
                rsp = Activator.CreateInstance<T>();
            }

            if (rsp != null)
            {
                rsp.Body = body;
            }

            return rsp;
        }
    }
}
