﻿using System;
using System.Runtime.Serialization;

namespace Ecloud.Ccs.Core
{
    /// <summary>
    /// Ec客户端异常。
    /// </summary>
    public class EcException : Exception
    {
        private string errorCode;
        private string errorMsg;
        private string subErrorCode;
        private string subErrorMsg;

        public EcException()
            : base()
        {
        }

        public EcException(string message)
            : base(message)
        {
        }

        protected EcException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public EcException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public EcException(string errorCode, string errorMsg)
            : base(errorCode + ":" + errorMsg)
        {
            this.errorCode = errorCode;
            this.errorMsg = errorMsg;
        }

        public EcException(string errorCode, string errorMsg, string subErrorCode, string subErrorMsg)
            : base(errorCode + ":" + errorMsg + ":" + subErrorCode + ":" + subErrorMsg)
        {
            this.errorCode = errorCode;
            this.errorMsg = errorMsg;
            this.subErrorCode = subErrorCode;
            this.subErrorMsg = subErrorMsg;
        }

        public string ErrorCode
        {
            get { return this.errorCode; }
        }

        public string ErrorMsg
        {
            get { return this.errorMsg; }
        }

        public string SubErrorCode
        {
            get { return this.subErrorCode; }
        }

        public string SubErrorMsg
        {
            get { return this.subErrorMsg; }
        }


    }
}
