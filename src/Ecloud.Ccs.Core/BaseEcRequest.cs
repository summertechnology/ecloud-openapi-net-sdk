﻿using System.Collections.Generic;
using Ecloud.Ccs.Core.Response;

namespace Ecloud.Ccs.Core
{
    /// <summary>
    /// 基础TOP请求类，存放一些通用的请求参数。
    /// </summary>
    public abstract class BaseEcRequest<T> : IEcRequest<T> where T : EcResponse
    {
        /// <summary>
        /// HTTP请求URL参数
        /// </summary>
        public EcDictionary otherParams;

        /// <summary>
        /// HTTP请求头参数
        /// </summary>
        private EcDictionary headerParams;
        

        public void AddOtherParameter(string key, string value)
        {
            if (this.otherParams == null)
            {
                this.otherParams = new EcDictionary();
            }
            this.otherParams.Add(key, value);
        }

        public void AddHeaderParameter(string key, string value)
        {
            GetHeaderParameters().Add(key, value);
        }

        public IDictionary<string, string> GetHeaderParameters()
        {
            if (this.headerParams == null)
            {
                this.headerParams = new EcDictionary();
            }
            return this.headerParams;
        }

        public abstract string GetApiName();

        public abstract void Validate();

        public abstract IDictionary<string, string> GetParameters();
    }
}
