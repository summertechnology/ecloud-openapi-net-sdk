﻿using System;
using Ecloud.Ccs.Core.Response;

namespace Ecloud.Ccs.Core
{
    /// <summary>
    /// Ec客户端。
    /// </summary>
    public interface IEcClient
    {
        /// <summary>
        /// 执行Ec公开API请求。
        /// </summary>
        /// <typeparam name="T">领域对象</typeparam>
        /// <param name="request">具体的Ec API请求</param>
        /// <returns>领域对象</returns>
        T Execute<T>(IEcRequest<T> request) where T : EcResponse, new();

        /// <summary>
        /// 执行Ec隐私API请求。
        /// </summary>
        /// <typeparam name="T">领域对象</typeparam>
        /// <param name="request">具体的Ec API请求</param>
        /// <param name="session">用户会话码</param>
        /// <returns>领域对象</returns>
        T Execute<T>(IEcRequest<T> request, string session) where T : EcResponse, new();

        /// <summary>
        /// 执行Ec隐私API请求。
        /// </summary>
        /// <typeparam name="T">领域对象</typeparam>
        /// <param name="request">具体的Ec API请求</param>
        /// <param name="session">用户会话码</param>
        /// <param name="timestamp">请求时间戳</param>
        /// <returns>领域对象</returns>
        T Execute<T>(IEcRequest<T> request, string session, DateTime timestamp) where T : EcResponse, new();
    }
}
