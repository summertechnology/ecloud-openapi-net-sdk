﻿using System.Xml.Serialization;

namespace Ecloud.Ccs.Core.Response
{
    [XmlRoot("login_response")]
    public class LoginResponse : EcResponse
    {
        /// <summary>
        /// 用户token值
        /// </summary>
        [XmlElement("token")]
        public string Token { get; set; }

        /// <summary>
        /// 用户id
        /// </summary>
        [XmlElement("user_id")]
        public int UserId { get; set; }

        /// <summary>
        /// 是否是第一次登录
        /// </summary>
        [XmlElement("first_login")]
        public bool? FirstLogin { get; set; }
    }
}
