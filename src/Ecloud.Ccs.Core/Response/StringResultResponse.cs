﻿using System.Xml.Serialization;

namespace Ecloud.Ccs.Core.Response
{
    [XmlRoot("string_result_response")]
    public class StringResultResponse : EcResponse
    {
        [XmlElement("string_result")]
        public string Result { get; set; }
    }
}
