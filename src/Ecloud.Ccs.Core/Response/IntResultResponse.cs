﻿using System.Xml.Serialization;

namespace Ecloud.Ccs.Core.Response
{
    [XmlRoot("number_result_response")]
    public class IntResultResponse : EcResponse
    {
        [XmlElement("number_result")]
        public long Result { get; set; }
    }
}
