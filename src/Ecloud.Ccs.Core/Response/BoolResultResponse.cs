﻿using System.Xml.Serialization;

namespace Ecloud.Ccs.Core.Response
{
    [XmlRoot("bool_result_response")]
    public class BoolResultResponse : EcResponse
    {
        [XmlElement("bool_result")]
        public bool Result { get; set; }
    }
}
