﻿using System;
using System.Collections.Generic;
using System.Xml;
using Ecloud.Ccs.Core.Parser;
using Ecloud.Ccs.Core.Response;
using Ecloud.Ccs.Core.Util;
using fastJSON;

namespace Ecloud.Ccs.Core
{
    /// <summary>
    /// 基于REST的Ec客户端。
    /// </summary>
    public class DefaultEcClient : IEcClient
    {
        internal string serverUrl;
        internal string appKey;
        internal string appSecret;
        internal string format = Constants.FORMAT_XML;

        internal WebUtils webUtils;
        internal IEcLogger EcLogger;
        internal bool disableParser = false; // 禁用响应结果解释
        internal bool disableTrace = false; // 禁用日志调试功能
        internal bool useSimplifyJson = false; // 是否采用精简化的JSON返回
        internal bool useGzipEncoding = true;  // 是否启用响应GZIP压缩
        internal IDictionary<string, string> systemParameters; // 设置所有请求共享的系统级参数

        #region DefaultEcClient Constructors

        public DefaultEcClient(string serverUrl, string appKey, string appSecret)
        {
            this.appKey = appKey;
            this.appSecret = appSecret;
            this.serverUrl = serverUrl;
            this.webUtils = new WebUtils();
            this.EcLogger = DefaultEcLogger.Instance;
        }

        public DefaultEcClient(string serverUrl, string appKey, string appSecret, string format)
            : this(serverUrl, appKey, appSecret)
        {
            this.format = format;
        }

        #endregion

        public void SetTimeout(int timeout)
        {
            this.webUtils.Timeout = timeout;
        }

        public void SetReadWriteTimeout(int readWriteTimeout)
        {
            this.webUtils.ReadWriteTimeout = readWriteTimeout;
        }

        public void SetDisableParser(bool disableParser)
        {
            this.disableParser = disableParser;
        }

        public void SetDisableTrace(bool disableTrace)
        {
            this.disableTrace = disableTrace;
        }

        public void SetUseSimplifyJson(bool useSimplifyJson)
        {
            this.useSimplifyJson = useSimplifyJson;
        }

        public void SetUseGzipEncoding(bool useGzipEncoding)
        {
            this.useGzipEncoding = useGzipEncoding;
        }

        public void SetIgnoreSSLCheck(bool ignore)
        {
            this.webUtils.IgnoreSSLCheck = ignore;
        }

        /// <summary>
        /// 禁用本地代理自动检测功能，加快连接建立速度
        /// </summary>
        public void SetDisableWebProxy(bool disable)
        {
            this.webUtils.DisableWebProxy = disable;
        }

        /// <summary>
        /// 设置单个网站最大并发连接数，桌面端默认是2，服务器端默认是10，对于桌面端并发请求多的，可以适当调高。
        /// </summary>
        /// <param name="limit"></param>
        public void SetMaxConnectionLimit(int limit)
        {
            System.Net.ServicePointManager.DefaultConnectionLimit = limit;
        }

        public void SetSystemParameters(IDictionary<string, string> systemParameters)
        {
            this.systemParameters = systemParameters;
        }

        #region IEcClient Members

        public virtual T Execute<T>(IEcRequest<T> request) where T : EcResponse, new()
        {
            return DoExecute<T>(request, null, DateTime.Now);
        }

        public virtual T Execute<T>(IEcRequest<T> request, string session) where T : EcResponse, new()
        {
            return DoExecute<T>(request, session, DateTime.Now);
        }

        public virtual T Execute<T>(IEcRequest<T> request, string session, DateTime timestamp) where T : EcResponse, new()
        {
            return DoExecute<T>(request, session, timestamp);
        }

        #endregion

        private T DoExecute<T>(IEcRequest<T> request, string session, DateTime timestamp) where T : EcResponse, new()
        {
            long start = DateTime.Now.Ticks;

            // 提前检查业务参数
            try
            {
                request.Validate();
            }
            catch (EcException e)
            {
                return CreateErrorResponse<T>(e.ErrorCode, e.ErrorMsg);
            }

            // 添加协议级请求参数
            var txtParams = new EcDictionary(request.GetParameters());

            if (!txtParams.ContainsKey(Constants.METHOD))
            {
                txtParams.Add(Constants.METHOD, request.GetApiName());
            }

            if (!txtParams.ContainsKey(Constants.VERSION))
            {
                txtParams.Add(Constants.VERSION, "2.0");
            }

            if (!txtParams.ContainsKey(Constants.APP_KEY))
            {
                txtParams.Add(Constants.APP_KEY, appKey);
            }

            if (!txtParams.ContainsKey(Constants.FORMAT))
            {
                txtParams.Add(Constants.FORMAT, format);
            }

            if (!txtParams.ContainsKey(Constants.TIMESTAMP))
            {
                txtParams.Add(Constants.TIMESTAMP, timestamp);
            }

            if (!txtParams.ContainsKey(Constants.SESSION))
            {
                txtParams.Add(Constants.SESSION, session);
            }

            if (!string.IsNullOrEmpty(appSecret))
            {
                txtParams.Add(Constants.SIGN_METHOD, Constants.SIGN_METHOD_MD5);
            }

            txtParams.AddAll(systemParameters);

            // 添加签名参数
            txtParams.Add(Constants.SIGN, EcUtils.SignEcRequest(txtParams, appSecret, Constants.SIGN_METHOD_MD5));

            // 添加头部参数
            if (this.useGzipEncoding)
            {
                request.GetHeaderParameters()[Constants.ACCEPT_ENCODING] = Constants.CONTENT_ENCODING_GZIP;
            }

            string realServerUrl = GetServerUrl(this.serverUrl, request.GetApiName(), session);
            string reqUrl = WebUtils.BuildRequestUrl(realServerUrl, txtParams);

            // 解释响应结果
            T rsp = new T {ReqUrl = reqUrl};

            try
            {
                string body;
                if (request is IEcUploadRequest<T>) // 是否需要上传文件
                {
                    var uRequest = (IEcUploadRequest<T>)request;
                    var fileParams = EcUtils.CleanupDictionary(uRequest.GetFileParameters());
                    body = webUtils.DoPost(realServerUrl, txtParams, fileParams, request.GetHeaderParameters());
                }
                else
                {
                    body = webUtils.DoPost(realServerUrl, txtParams, request.GetHeaderParameters());
                }

                if (disableParser)
                {
                    rsp = Activator.CreateInstance<T>();
                }
                else
                {
                    if (Constants.FORMAT_XML.Equals(format))
                    {
                        IEcParser<T> tp = new EcXmlParser<T>();
                        rsp = tp.Parse(body);
                    }
                    else
                    {
                        IEcParser<T> tp;
                        if (useSimplifyJson)
                        {
                            tp = new EcSimplifyJsonParser<T>();
                        }
                        else
                        {
                            tp = new EcJsonParser<T>();
                        }
                        rsp = tp.Parse(body);
                    }
                }

                rsp.ReqUrl = reqUrl;
                rsp.Body = body;

                // 追踪错误的请求
                if (rsp.IsError)
                {
                    TimeSpan latency = new TimeSpan(DateTime.Now.Ticks - start);
                    TraceApiError(appKey, request.GetApiName(), serverUrl, txtParams, latency.TotalMilliseconds, rsp.Body);
                }

                return rsp;
            }
            catch (Exception e)
            {
                TimeSpan latency = new TimeSpan(DateTime.Now.Ticks - start);
                TraceApiError(appKey, request.GetApiName(), serverUrl, txtParams, latency.TotalMilliseconds, e.GetType() + ": " + e.Message);

                rsp.Body = e.Message;
                return rsp;
            }
        }

        internal virtual string GetServerUrl(string serverUrl, string apiName, string session)
        {
            return serverUrl;
        }

        internal T CreateErrorResponse<T>(string errCode, string errMsg) where T : EcResponse
        {
            T rsp = Activator.CreateInstance<T>();
            rsp.ErrCode = errCode;
            rsp.ErrMsg = errMsg;

            if (Constants.FORMAT_XML.Equals(format))
            {
                XmlDocument root = new XmlDocument();
                XmlElement bodyE = root.CreateElement(Constants.ERROR_RESPONSE);
                XmlElement codeE = root.CreateElement(Constants.ERROR_CODE);
                codeE.InnerText = errCode;
                bodyE.AppendChild(codeE);
                XmlElement msgE = root.CreateElement(Constants.ERROR_MSG);
                msgE.InnerText = errMsg;
                bodyE.AppendChild(msgE);
                root.AppendChild(bodyE);
                rsp.Body = root.OuterXml;
            }
            else
            {
                IDictionary<string, object> errObj = new Dictionary<string, object>();
                errObj.Add(Constants.ERROR_CODE, errCode);
                errObj.Add(Constants.ERROR_MSG, errMsg);
                IDictionary<string, object> root = new Dictionary<string, object>();
                root.Add(Constants.ERROR_RESPONSE, errObj);

                string body = JSON.ToJSON(root);
                rsp.Body = body;
            }
            return rsp;
        }

        internal void TraceApiError(string appKey, string apiName, string url, Dictionary<string, string> parameters, double latency, string errorMessage)
        {
            if (!disableTrace)
            {
                this.EcLogger.TraceApiError(appKey, apiName, url, parameters, latency, errorMessage);
            }
        }
    }
}
