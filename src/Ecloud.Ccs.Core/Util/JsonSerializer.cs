﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;

namespace Ecloud.Ccs.Core.Util
{
    public class JsonSerializer
    {
        #region Functions

        /// <summary>
        /// Serializes the object
        /// </summary>
        /// <param name="Object">Object to serialize</param>
        /// <returns>The serialized object</returns>
        public static string Serialize(object Object)
        {
            //return JsonConvert.SerializeObject(Object);

            if (Object == null) return "";

            using (var stream = new MemoryStream())
            {
                var serializer = new DataContractJsonSerializer(Object.GetType(), new DataContractJsonSerializerSettings
                {
                    DateTimeFormat = new DateTimeFormat("yyyy-MM-dd HH:mm:ss")
                });

                serializer.WriteObject(stream, Object);
                stream.Flush();
                return Encoding.UTF8.GetString(stream.GetBuffer(), 0, (int) stream.Position);
            }
        }

        /// <summary>
        /// Deserializes the data
        /// </summary>
        /// <param name="data">Data to deserialize</param>
        /// <returns>The resulting object</returns>
        public static T Deserialize<T>(string data)
        {
            if (string.IsNullOrWhiteSpace(data))
                return default(T);

            using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(data)))
            {
                var serializer = new DataContractJsonSerializer(typeof(T), new DataContractJsonSerializerSettings
                {
                    DateTimeFormat = new DateTimeFormat("yyyy-MM-dd HH:mm:ss")
                });

                return (T) serializer.ReadObject(stream);
            }
        }

        #endregion
    }
}
