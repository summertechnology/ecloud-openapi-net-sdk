using Ecloud.Ccs.Core.Response;
using System.Collections.Generic;
using System.Xml.Serialization;
using Ecloud.Ccs.ApiGateway.Log.Domain;

namespace Ecloud.Ccs.ApiGateway.Log.Response
{
    [XmlRoot("add_system_runtime_log_response")]
    public class AddSystemRuntimeLogResponse : EcResponse
    {
        /// <summary>
        /// 通用的结果码，不同方法所代表的返回值意义不同
        /// </summary>
        [XmlElement("number_result")]
        public long NumberResult { get; set; }



    }
}