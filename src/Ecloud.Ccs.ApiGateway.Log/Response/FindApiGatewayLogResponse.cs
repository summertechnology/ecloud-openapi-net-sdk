using System;
using Ecloud.Ccs.Core.Response;
using System.Collections.Generic;
using System.Xml.Serialization;
using Ecloud.Ccs.ApiGateway.Log.Domain;

namespace Ecloud.Ccs.ApiGateway.Log.Response
{
    [XmlRoot("find_api_gateway_log_response")]
    public class FindApiGatewayLogResponse : EcResponse
    {
        /// <summary>
        /// 总数
        /// </summary>
        [XmlElement("total_item")]
        public long TotalItem { get; set; }

        /// <summary>
        /// 网关日志列表
        /// </summary>
        [XmlArray("api_gateway_log_detail_list")]
        [XmlArrayItem("api_gateway_log_detail")]
        public List<ApiGatewayLogDetailDomain> ApiGatewayLogDetailList { get; set; }



    }
}