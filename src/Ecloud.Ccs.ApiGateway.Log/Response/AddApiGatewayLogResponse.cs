using Ecloud.Ccs.Core.Response;
using System.Collections.Generic;
using System.Xml.Serialization;
using Ecloud.Ccs.ApiGateway.Log.Domain;

namespace Ecloud.Ccs.ApiGateway.Log.Response
{
    [XmlRoot("add_api_gateway_log_response")]
    public class AddApiGatewayLogResponse : EcResponse
    {
        /// <summary>
        /// 通用的结果码，不同方法所代表的返回值意义不同
        /// </summary>
        [XmlElement("number_result")]
        public long NumberResult { get; set; }



    }
}