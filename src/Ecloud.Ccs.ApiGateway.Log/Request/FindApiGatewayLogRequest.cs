using System;
using System.Collections.Generic;
using Ecloud.Ccs.Core.Util;
using Ecloud.Ccs.Core.Response;
using Ecloud.Ccs.Core;
using Ecloud.Ccs.ApiGateway.Log.Domain;
using Ecloud.Ccs.ApiGateway.Log.Response;

namespace Ecloud.Ccs.ApiGateway.Log.Request
{
    /// <summary>
    /// 查找接口日志
    /// </summary>
    public class FindApiGatewayLogRequest : BaseEcRequest<FindApiGatewayLogResponse>
    {
        /// <summary>
        /// 页容量
        /// </summary>
        public long? PageSize { get; set; }

        /// <summary>
        /// 页码
        /// </summary>
        public long? PageNum { get; set; }



        public override string GetApiName()
        {
            return "apigateway.log.find";
        }

        public override void Validate()
        {
        }

        public override IDictionary<string, string> GetParameters()
        {
            var parameters = new EcDictionary();
            parameters.Add("page_size", PageSize);
            parameters.Add("page_num", PageNum);

            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }

            return parameters;
        }

    }
}