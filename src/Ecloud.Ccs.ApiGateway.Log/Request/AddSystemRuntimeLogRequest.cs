using System;
using System.Collections.Generic;
using Ecloud.Ccs.Core.Util;
using Ecloud.Ccs.Core.Response;
using Ecloud.Ccs.Core;
using Ecloud.Ccs.ApiGateway.Log.Domain;
using Ecloud.Ccs.ApiGateway.Log.Response;

namespace Ecloud.Ccs.ApiGateway.Log.Request
{
    /// <summary>
    /// 新增系统运行日志
    /// </summary>
    public class AddSystemRuntimeLogRequest : BaseEcRequest<IntResultResponse>
    {
        /// <summary>
        /// 客户端主机
        /// </summary>
        public string ClientHost { get; set; }

        /// <summary>
        /// 日志产生时间
        /// </summary>
        public DateTime? LogCreateTime { get; set; }

        /// <summary>
        /// 自定义分类
        /// </summary>
        public string Cat { get; set; }

        /// <summary>
        /// 日志内容
        /// </summary>
        public string Msg { get; set; }

        /// <summary>
        /// 正在运行的方法名
        /// </summary>
        public string ClassName { get; set; }



        public override string GetApiName()
        {
            return "apigateway.system.runtime.log.add";
        }

        public override void Validate()
        {
        }

        public override IDictionary<string, string> GetParameters()
        {
            var parameters = new EcDictionary();
            parameters.Add("client_host", ClientHost);
            parameters.Add("log_create_time", LogCreateTime);
            parameters.Add("cat", Cat);
            parameters.Add("msg", Msg);
            parameters.Add("class_name", ClassName);

            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }

            return parameters;
        }

    }
}