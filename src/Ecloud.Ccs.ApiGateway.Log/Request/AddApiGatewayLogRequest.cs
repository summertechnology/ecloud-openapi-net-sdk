using System;
using System.Collections.Generic;
using Ecloud.Ccs.Core.Util;
using Ecloud.Ccs.Core.Response;
using Ecloud.Ccs.Core;
using Ecloud.Ccs.ApiGateway.Log.Domain;
using Ecloud.Ccs.ApiGateway.Log.Response;

namespace Ecloud.Ccs.ApiGateway.Log.Request
{
    /// <summary>
    /// 新增接口日志
    /// </summary>
    public class AddApiGatewayLogRequest : BaseEcRequest<IntResultResponse>
    {
        /// <summary>
        /// 网关日志
        /// </summary>
        public ApiGatewayLogDomain ApiGatewayLog { get; set; }



        public override string GetApiName()
        {
            return "apigateway.log.add";
        }

        public override void Validate()
        {
        }

        public override IDictionary<string, string> GetParameters()
        {
            var parameters = new EcDictionary();
            parameters.Add("api_gateway_log", ApiGatewayLog);

            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }

            return parameters;
        }

    }
}