using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Ecloud.Ccs.ApiGateway.Log.Domain
{
    [Serializable]
    [DataContract]
    public class ApiGatewayLogDetailDomain
    {
        /// <summary>
        /// 网关服务器名称
        /// </summary>
        [XmlElement("gateway_server")]
        [DataMember(Name = "gateway_server")]
        public string GatewayServer { get; set; }

        /// <summary>
        /// 网关服务URL
        /// </summary>
        [XmlElement("service_url")]
        [DataMember(Name = "service_url")]
        public string ServiceUrl { get; set; }

        /// <summary>
        /// 应用级错误
        /// </summary>
        [XmlElement("application_error")]
        [DataMember(Name = "application_error")]
        public string ApplicationError { get; set; }

        /// <summary>
        /// IP地址
        /// </summary>
        [XmlElement("ip_address")]
        [DataMember(Name = "ip_address")]
        public string IpAddress { get; set; }

        /// <summary>
        /// 返回内容
        /// </summary>
        [XmlElement("result")]
        [DataMember(Name = "result")]
        public string Result { get; set; }

        /// <summary>
        /// 请求ID
        /// </summary>
        [XmlElement("request_id")]
        [DataMember(Name = "request_id")]
        public string RequestId { get; set; }

        /// <summary>
        /// 执行时间
        /// </summary>
        [XmlElement("spend_time")]
        [DataMember(Name = "spend_time")]
        public long SpendTime { get; set; }

        /// <summary>
        /// 状态值
        /// </summary>
        [XmlElement("state")]
        [DataMember(Name = "state")]
        public long State { get; set; }

        /// <summary>
        /// 请求url参数
        /// </summary>
        [XmlElement("url")]
        [DataMember(Name = "url")]
        public string Url { get; set; }

        /// <summary>
        /// 数据堆ID
        /// </summary>
        [XmlElement("site_id")]
        [DataMember(Name = "site_id")]
        public long SiteId { get; set; }

        /// <summary>
        /// 版本
        /// </summary>
        [XmlElement("v")]
        [DataMember(Name = "v")]
        public string V { get; set; }

        /// <summary>
        /// 方法名
        /// </summary>
        [XmlElement("method")]
        [DataMember(Name = "method")]
        public string Method { get; set; }

        /// <summary>
        /// 父级数据堆ID
        /// </summary>
        [XmlElement("parent_site_id")]
        [DataMember(Name = "parent_site_id")]
        public long ParentSiteId { get; set; }

        /// <summary>
        /// 请求方式，POST或者GET
        /// </summary>
        [XmlElement("request_type")]
        [DataMember(Name = "request_type")]
        public string RequestType { get; set; }


    }
}