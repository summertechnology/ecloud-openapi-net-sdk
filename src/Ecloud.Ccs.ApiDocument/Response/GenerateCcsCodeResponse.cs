using System;
using Ecloud.Ccs.Core.Response;
using System.Collections.Generic;
using System.Xml.Serialization;
using Ecloud.Ccs.ApiDocument.Domain;

namespace Ecloud.Ccs.ApiDocument.Response
{
    [XmlRoot("generate_ccs_code_response")]
    public class GenerateCcsCodeResponse : EcResponse
    {
        /// <summary>
        /// 下载地址
        /// </summary>
        [XmlElement("download_url")]
        public string DownloadUrl { get; set; }



    }
}