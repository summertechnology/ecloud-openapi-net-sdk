using Ecloud.Ccs.Core.Response;
using System.Collections.Generic;
using System.Xml.Serialization;
using Ecloud.Ccs.ApiDocument.Domain;

namespace Ecloud.Ccs.ApiDocument.Response
{
    [XmlRoot("save_api_document_response")]
    public class SaveApiDocumentResponse : EcResponse
    {
        /// <summary>
        /// 通用的结果码，不同方法所代表的返回值意义不同
        /// </summary>
        [XmlElement("number_result")]
        public long NumberResult { get; set; }



    }
}