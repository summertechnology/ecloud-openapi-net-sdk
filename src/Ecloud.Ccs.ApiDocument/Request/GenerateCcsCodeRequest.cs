using System;
using System.Collections.Generic;
using Ecloud.Ccs.Core.Util;
using Ecloud.Ccs.Core.Response;
using Ecloud.Ccs.Core;
using Ecloud.Ccs.ApiDocument.Domain;
using Ecloud.Ccs.ApiDocument.Response;

namespace Ecloud.Ccs.ApiDocument.Request
{
    /// <summary>
    /// 生成不同语言的SDK代码
    /// </summary>
    public class GenerateCcsCodeRequest : BaseEcRequest<GenerateCcsCodeResponse>
    {
        /// <summary>
        /// 开发语言，支持php,java,net,python
        /// </summary>
        public string Language { get; set; }



        public override string GetApiName()
        {
            return "apidocument.ccscode.generate";
        }

        public override void Validate()
        {
        }

        public override IDictionary<string, string> GetParameters()
        {
            var parameters = new EcDictionary();
            parameters.Add("language", Language);

            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }

            return parameters;
        }

    }
}