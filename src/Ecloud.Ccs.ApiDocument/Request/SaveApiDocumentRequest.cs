using System;
using System.Collections.Generic;
using Ecloud.Ccs.Core.Util;
using Ecloud.Ccs.Core.Response;
using Ecloud.Ccs.Core;
using Ecloud.Ccs.ApiDocument.Domain;
using Ecloud.Ccs.ApiDocument.Response;

namespace Ecloud.Ccs.ApiDocument.Request
{
    /// <summary>
    /// 保存接口文档
    /// </summary>
    public class SaveApiDocumentRequest : BaseEcRequest<IntResultResponse>
    {
        /// <summary>
        /// 接口方法请求参数和返回参数
        /// </summary>
        public List<ApiMethodParameterDomain> ApiMethodParms { get; set; }

        /// <summary>
        /// 接口方法
        /// </summary>
        public ApiMethodDomain ApiMethod { get; set; }



        public override string GetApiName()
        {
            return "apidocument.save";
        }

        public override void Validate()
        {
        }

        public override IDictionary<string, string> GetParameters()
        {
            var parameters = new EcDictionary();
            parameters.Add("api_method_parms", ApiMethodParms);
            parameters.Add("api_method", ApiMethod);

            if (this.otherParams != null)
            {
                parameters.AddAll(this.otherParams);
            }

            return parameters;
        }

    }
}