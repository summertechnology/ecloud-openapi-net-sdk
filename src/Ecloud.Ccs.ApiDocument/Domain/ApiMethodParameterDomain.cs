using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Ecloud.Ccs.ApiDocument.Domain
{
    [Serializable]
    [DataContract]
    public class ApiMethodParameterDomain
    {
        /// <summary>
        /// 系统类型，0：请求参数，1：返回参数
        /// </summary>
        [XmlElement("system_type")]
        [DataMember(Name = "system_type")]
        public long SystemType { get; set; }

        /// <summary>
        /// 父ID
        /// </summary>
        [XmlElement("parent_id")]
        [DataMember(Name = "parent_id")]
        public string ParentId { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [XmlElement("description")]
        [DataMember(Name = "description")]
        public string Description { get; set; }

        /// <summary>
        /// 是否必须
        /// </summary>
        [XmlElement("required")]
        [DataMember(Name = "required")]
        public bool Required { get; set; }

        /// <summary>
        /// 示例值
        /// </summary>
        [XmlElement("sample_value")]
        [DataMember(Name = "sample_value")]
        public string SampleValue { get; set; }

        /// <summary>
        /// 参数名
        /// </summary>
        [XmlElement("name")]
        [DataMember(Name = "name")]
        public string Name { get; set; }

        /// <summary>
        /// 类型名字
        /// </summary>
        [XmlElement("type_name")]
        [DataMember(Name = "type_name")]
        public string TypeName { get; set; }

        /// <summary>
        /// id
        /// </summary>
        [XmlElement("id")]
        [DataMember(Name = "id")]
        public string Id { get; set; }

        /// <summary>
        /// 方法ID
        /// </summary>
        [XmlElement("method_id")]
        [DataMember(Name = "method_id")]
        public string MethodId { get; set; }

        /// <summary>
        /// 更多限制
        /// </summary>
        [XmlElement("more_restrictions")]
        [DataMember(Name = "more_restrictions")]
        public string MoreRestrictions { get; set; }


    }
}