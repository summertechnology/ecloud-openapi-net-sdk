using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Ecloud.Ccs.ApiDocument.Domain
{
    [Serializable]
    [DataContract]
    public class ApiMethodDomain
    {
        /// <summary>
        /// 模块ID
        /// </summary>
        [XmlElement("module_id")]
        [DataMember(Name = "module_id")]
        public long ModuleId { get; set; }

        /// <summary>
        /// 不公开
        /// </summary>
        [XmlElement("not_public")]
        [DataMember(Name = "not_public")]
        public bool NotPublic { get; set; }

        /// <summary>
        /// 必须授权
        /// </summary>
        [XmlElement("must_authorized")]
        [DataMember(Name = "must_authorized")]
        public bool MustAuthorized { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [XmlElement("create_time")]
        [DataMember(Name = "create_time")]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [XmlElement("description")]
        [DataMember(Name = "description")]
        public string Description { get; set; }

        /// <summary>
        /// 完整类名
        /// </summary>
        [XmlElement("class_name")]
        [DataMember(Name = "class_name")]
        public string ClassName { get; set; }

        /// <summary>
        /// 响应类型名字
        /// </summary>
        [XmlElement("response_type_name")]
        [DataMember(Name = "response_type_name")]
        public string ResponseTypeName { get; set; }

        /// <summary>
        /// 程序集
        /// </summary>
        [XmlElement("assembly")]
        [DataMember(Name = "assembly")]
        public string Assembly { get; set; }

        /// <summary>
        /// 版本号
        /// </summary>
        [XmlElement("v")]
        [DataMember(Name = "v")]
        public string V { get; set; }

        /// <summary>
        /// 方法名
        /// </summary>
        [XmlElement("method")]
        [DataMember(Name = "method")]
        public string Method { get; set; }

        /// <summary>
        /// 详细介绍
        /// </summary>
        [XmlElement("explain")]
        [DataMember(Name = "explain")]
        public string Explain { get; set; }

        /// <summary>
        /// id
        /// </summary>
        [XmlElement("id")]
        [DataMember(Name = "id")]
        public string Id { get; set; }


    }
}